Original idea from quintana

Change ENV XUC_VERSION 2.3.11 to current xuc version

Copy deb file localy

Build by :

docker build -t xuc2.3.11  .

Run using : 

docker run -d -v <localconfigfilepath>:/conf/ -e CONFIG_FILE="/conf/application.conf" -e NO_DAEMON="Y" -p 9000:9000 xuc2.3.11

